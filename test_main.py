import pytest
from main import callerId

def test_name_found():
    names = ["john dow"]
    numbers = ["111-222-333"]  
    assert names[0] == callerId(numbers, names, numbers[0])

def test_name_not_found():
    names = ["john dow"]
    numbers = ["111-222-333"]  
    number = "000-000-000"
    assert number == callerId(numbers, names, number)

def test_empty_init_name_not_found():
    names = []
    numbers = []  
    number = "000-000-000"
    assert number == callerId(numbers, names, number)

