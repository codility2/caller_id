During an incoming call, phones usually display on the screen the number of the caller. If the number was previously saved in the contacts, the phone displays the name of the caller from the contact list instead. Your task is to write a function which determines what should be displayed on the phone screen during an incoming call. 
  
Write a function: that, given the contact list in the form of two arrays, phone numbers and phone owners, consisting of N strings each, and a string number denoting the caller's number, retums the name of corresponding contact, or just number if the number is not present in the contact list. phone_numbers[a] contains the phone number of the person from phone_owners[S], where T is within the range [0..N -1]. 
  
Examples:
1. Given phone_numbers = r234-567-890", '444-444-444", '321-543-2341 phone_owners = ["Harry", "Michaell and number ="444-444-444", your function should return "Nick'. 
2. Given phone_numbers = 23-123-1231,phone_owners = ['Walter] and number = "1 1 -1 1 1 -111", your function should return "111-111-111". 
3. Given phone_numbers = 23-456-1231 phone_owners = ["Henry T."] and number ='123-456-123", your function should return "Henry T". 
4. Given phone_numbers = -111-112", '211-111-1111 phone_owner s = ['laundry', "call center.] and number = "141-114-111% your function should return '111-111-111". 
  
Assume that:  
• N is an integer within the range [1..1,000];  
• the length of each string of array phone owners is within the range [1..100];   
• phone numbers and phone _owners are both of length N; • every phone number follows the format 'nnn-nnn-nnn".   
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.   
