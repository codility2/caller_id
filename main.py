from typing import List

def callerId(phone_nrs: List[str], names: List[str], number: str) -> str:
    return dict(zip(phone_nrs, names)).get(number, number)

